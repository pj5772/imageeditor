package com.pjmoat.imageeditor.imageeditor;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.ColorInt;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.pes.androidmaterialcolorpickerdialog.ColorPicker;
import com.pes.androidmaterialcolorpickerdialog.ColorPickerCallback;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    String TAG = "com.pjmoat.imageeditor.imageeditor";

    //hold all icons
    private ArrayList<Icon> icons;

    //icons recyclerview + adapter
    private IconsAdapter adapter;
    RecyclerView iconsRecyclerView;

    private Bitmap currentImage;

    ImageView currentImageView;

    //starting drag location
    float[] dragStart = {-1,-1};

    //request code for intent when user selects photo from gallery
    int IMAGE_PICKER_SELECT = 1;

    //resource for asking permission to read/write phone's storage
    private static final int MY_PERMISSIONS_REQUEST_RW_EXTERNAL_STORAGE = 2;

    //resouce for crop image intent
    final int PIC_CROP = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //hide actionbar
        getSupportActionBar().hide();

        //if we don't have permission, get it
        if (!havePermission()) {
            getPermission();
            return;
        }

        //setup Activity
        setupActivity();

        //setup imageview
        currentImageView = (ImageView) findViewById(R.id.current_imageview);

        //decompress sent image
        if(getIntent().hasExtra("PATH_TO_IMAGE")) {

            //get image's path
            String img_path = (String) getIntent().getExtras().get("PATH_TO_IMAGE");

            //decode bitmap
            File imgFile = new  File(img_path);
            Bitmap loadedBitmap = null;
            if(imgFile.exists()){
                loadedBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            }

            //set to current image
            if (loadedBitmap != null) {
                setImage(loadedBitmap);
            }
        }
    }

    private void createIconsView() {
        //create icons
        createIcons();

        //get reference to recyclerview showing icons
        iconsRecyclerView = findViewById(R.id.icons_recyclerview);

        //create layout manager to orient the icons horizontally
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);

        //specify layout manager
        iconsRecyclerView.setLayoutManager(layoutManager);

        //create adpater to manage icons
        adapter = new IconsAdapter(this, icons);

        //attach adapter to icons recyclerview
        iconsRecyclerView.setAdapter(adapter);

        //setup onclick listener
        iconsRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(this, iconsRecyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        handleAction(icons.get(position).action);
                    }

                    @Override public void onLongItemClick(View view, int position) {
                        //handle long click just like regular click
                        onItemClick(view,position);
                    }
                })
        );
    }

    //when passed an icon action {OPEN,SAVE,NEW,SHARE,FILTER,CROP,ROTATE,RESIZE}
    //this method does that action.
    private void handleAction(IconAction action) {
        switch (action) {
            case OPEN:
                //start intent that allows user to choose an image from his gallery
                Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                pickIntent.setType("image/* video/*");
                startActivityForResult(pickIntent,IMAGE_PICKER_SELECT);
                break;
            case SAVE:
                saveCurrentImage();
                break;
            case NEW:
                createNewImage();
                break;
            case SHARE:
                shareCurrentImage();
                break;
            case FILTER:
                filterCurrentImage();
                break;
            case CROP:
                cropCurrentImage();
                break;
            case ROTATE:
                rotateImage();
                break;
            case RESIZE:
                resizeImage();
                break;
        }
    }

    //method called when user selects the dimensions he/she wants to
    //scale the image to.
    private void onScaledImageParamsRecieved(int newWidth, int newHeight) {
        //create scaled bitmap from currentImage
        Bitmap scaledImage = Bitmap.createScaledBitmap(currentImage, newWidth, newHeight, false);

        //set to currentImage
        setImage(scaledImage);
    }

    //scales current image. Dialog will open for user to select a new size
    private void resizeImage() {
        //if no image is open there is nothing to scale
        if (currentImage == null) {
            Toast.makeText(this,"No image to scale.",Toast.LENGTH_SHORT).show();
            return;
        }

        //create dialog asking user to select width and height of image
        SelectSizeDialog selectSizeDialog = new SelectSizeDialog(MainActivity.this, new SelectSizeDialog.OnEnterClickListener() {
            @Override
            public void onEnterClicked(int width, int height) {

                //when new width and height is recieved, call onScaledImageParamsRecieved
                onScaledImageParamsRecieved(width,height);
            }
        });

        //set background color to black
        selectSizeDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.BLACK));

        //show dialog
        selectSizeDialog.show();
    }

    //rotates current image
    private void rotateImage() {
        //if no image is open, there is nothing to rotate
        if (currentImage == null) {
            Toast.makeText(this,"No image to rotate.",Toast.LENGTH_SHORT).show();
            return;
        }

        //setup matrix to rate by 90 deg
        Matrix matrix = new Matrix();
        matrix.postRotate(90);

        //create rotated bitmap
        Bitmap rotatedBitmap = Bitmap.createBitmap(currentImage, 0, 0, currentImage.getWidth(), currentImage.getHeight(), matrix, true);

        //set current image to rotated image
        setImage(rotatedBitmap);
    }

    //called when image is compressed for croping
    private void onImageCompressedForCrop(String path) {
        //create intent to send user to crop activity
        Intent i = new Intent(MainActivity.this,CropActivity.class);

        //only want user to be able to open one crop activity
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        //add compressed file path
        i.putExtra("PATH_TO_IMAGE", path);

        //start crop activity
        startActivity(i);
    }

    //when called will launch crop image activity with current image
    private void cropCurrentImage() {
        //if no image is open exit
        if (currentImage == null) {
            Toast.makeText(this,"No image to crop.",Toast.LENGTH_SHORT).show();
            return;
        }

        //compress image
        CompressImageTask compressImageTask = new CompressImageTask() {
            @Override
            protected void onPostExecute(String result) {

                //when compressed call onImageCompressed
                onImageCompressedForCrop(path);
            }
        };
        compressImageTask.execute(currentImage);
    }

    //handles everything involved with applying a color filter to an image.
    //this method opens a colorpickerdialog, then when user selects a color
    //onUserSelectedColor is called
    private void filterCurrentImage() {

        //if no image is open when cannot apply filter
        if (currentImage == null) {
            Toast.makeText(this,"No image to filter.",Toast.LENGTH_SHORT).show();
            return;
        }

        //display color picker for user to pick color
        final ColorPicker cp = new ColorPicker(MainActivity.this, 255, 0, 0);

        /* Show color picker dialog */
        cp.show();

        cp.enableAutoClose(); // Enable auto-dismiss for the dialog

        /* Set a new Listener called when user click "select" */
        cp.setCallback(new ColorPickerCallback() {
            @Override
            public void onColorChosen(@ColorInt int color) {
                // Do whatever you want
                // Examples
                onUserSelectedColor(color);

                // If the auto-dismiss option is not enable (disabled as default) you have to manually dimiss the dialog
                // cp.dismiss();
            }
        });
    }

    //called when user selects a color in a colorpickerdialog
    private void onUserSelectedColor(int color) {

        Paint paint = new Paint();
        ColorFilter filter = new PorterDuffColorFilter(color , PorterDuff.Mode.ADD);
        paint.setColorFilter(filter);

        Bitmap currentImageCopy = currentImage.copy(Bitmap.Config.ARGB_8888,true);
        Canvas canvas = new Canvas(currentImageCopy);
        canvas.drawBitmap(currentImageCopy, 0, 0, paint);

        setImage(currentImageCopy);
    }

    //called when user presses share button
    private void shareCurrentImage() {
        if (currentImage == null) {
            Toast.makeText(this,"No Image to share.",Toast.LENGTH_SHORT).show();
            return;
        }

        //compress image
        CompressImageTask compressImageTask = new CompressImageTask() {
            @Override
            protected void onPostExecute(String result) {

                //when compressed call onImageCompressed
                onImageCompressedForShare(path);
            }
        };
        compressImageTask.execute(currentImage);
    }

    //called by shareCurrentImage when image is compressed and ready to be used in implicit intent
    private void onImageCompressedForShare(String compressed_file_path) {
        //create intent to handle sharing image
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("image/jpeg");

        //open compressed file
        File f = new File(compressed_file_path);

        //get URi to access this file (uses GenericFileProvider)
        Uri photoURI = FileProvider.getUriForFile(this, this.getApplicationContext().getPackageName() + ".provider",f);

        //add this image's URi to intent
        shareIntent.putExtra(Intent.EXTRA_STREAM, photoURI);

        //this intent caries an image
        shareIntent.setType("image/jpeg");

        //display chooser so user can select how to share this song
        startActivity(Intent.createChooser(shareIntent, "How to Send?"));
    }

    //called when userpresses enter in SelectSizeDialog
    private void onNewImageParamsRecieved(int width, int height) {

        //exit if user entered to large of an image
        if (width > 2047 && height > 2047) {
            Toast.makeText(this,
                           "width " + width + "px and height " + height
                                            + "px are too large, max size is 2047",
                            Toast.LENGTH_LONG).show();
            return;
        }
        else if (width > 2047) {
            Toast.makeText(this,
                    "width " + width + "px is too large, max size is 2047",
                    Toast.LENGTH_LONG).show();
            return;
        }
        else if (height > 2047) {
            Toast.makeText(this,
                    "height " + height + "px is too large, max size is 2047",
                    Toast.LENGTH_LONG).show();
            return;
        }

        //creates blank bitmap with new width and height
        Bitmap.Config conf = Bitmap.Config.ARGB_8888;
        Bitmap newImage = Bitmap.createBitmap(width, height, conf);
        Canvas canvas = new Canvas(newImage);
        canvas.drawColor(0xffffffff);

        //setup new image as current image
        setImage(newImage);
    }

    //when called user is prompted to create new image
    private void createNewImage() {
        //create dialog asking user to select width and height of ne object
        SelectSizeDialog selectSizeDialog = new SelectSizeDialog(MainActivity.this, new SelectSizeDialog.OnEnterClickListener() {
            @Override
            public void onEnterClicked(int width, int height) {

                //when new width and height is recieved, call onNewImageParamsRecieved
                onNewImageParamsRecieved(width,height);
            }
        });

        //set background color to black
        selectSizeDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.BLACK));

        //show dialog
        selectSizeDialog.show();
    }

    //saves current image to users device
    private void saveCurrentImage() {

        //if no image is open
        if (currentImage == null) {

            //tell user he is trying to save an empty image
            Toast.makeText(this, "No image to save.",
                    Toast.LENGTH_LONG).show();

            //exit since nothing to save
            return;
        }

        //create salt for file name
        String salt = Integer.toString(new Random().nextInt(100_000));

        //get hashCode of image
        String img_hash = Integer.toString(currentImage.hashCode());

        //current date and time as string
        String currentDateTime = new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date());

        //combine to form unique id
        String unique_string = currentDateTime + img_hash + salt;

        //hash unique_id
        String hashText = "";
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            messageDigest.update(unique_string.getBytes());
            hashText = new String(messageDigest.digest());
        }
        catch (NoSuchAlgorithmException e) {
            Log.e(TAG,"got NoSuchAlgorithmException while saving file.");
        }

        //convert hash text to base-16 (unsigned hence 'b & 0xFF')
        StringBuilder hashText_16 = new StringBuilder();
        for (byte b : hashText.getBytes()) {
            hashText_16.append(Integer.toString((int) (b & 0xFF), 16));
        }

        //first 16 characters of base-16 hash text will be filename
        String filename = hashText_16.substring(0,16);

        //tell user file is  being saved
        Toast.makeText(this,"saved image as \"" + filename + ".jpg\"", Toast.LENGTH_SHORT).show();

        //save file
        MediaStore.Images.Media.insertImage(getContentResolver(),
                currentImage,
                filename
                ,"created with PJ's ImageEditor App");
    }

    //handles the result when user selects an image to open
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK) {
            Uri selectedMediaUri = data.getData();

            //if user selected a image, update ImageHolder with the new current image
            if (selectedMediaUri.toString().contains("image")) {

                //get Uri of selected image
                Uri selectedImageUri = data.getData();

                Bitmap selected_image = null;

                //get path from Uri
                try {
                    InputStream inputStream = this.getContentResolver().openInputStream(selectedImageUri);
                    selected_image = BitmapFactory.decodeStream(inputStream);
                    if (inputStream != null) inputStream.close();
                }
                catch (FileNotFoundException e) {
                    Log.e(TAG,"could not open file: " + selectedImageUri.getPath());
                }
                catch (IOException e) {
                    Log.e(TAG,"IOException occured while opening file: " + selectedImageUri.getPath());
                }

                //user has indeed selected an image
                if (selected_image != null) {

                    //update current image to selected_image
                    setImage(selected_image);
                }
            }

            //if user selected a video tell him/her to open an image
            else if (selectedMediaUri.toString().contains("video")) {
                Toast.makeText(this, "Cannot edit videos, please select an image!!",
                        Toast.LENGTH_LONG).show();
            }
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        //if we already have permission, or this is not the request we care about exit
        if (requestCode != MY_PERMISSIONS_REQUEST_RW_EXTERNAL_STORAGE)
            return;

        //did we get all the permissions we asked for
        boolean hasPermissions = true;
        for (int i = 0; i < permissions.length; i++)
            hasPermissions = hasPermissions && (grantResults[i] == PackageManager.PERMISSION_GRANTED);

        //exit if we don't have permission
        if (hasPermissions)
            setupActivity();
    }

    //sets up all the things that need to be setup up for this app
    private void setupActivity() {
        //create icons RecyclerView, if not already set up
        if (iconsRecyclerView == null)
            createIconsView();
    }

    //returns whether we have READ_EXTERNAL_STORAGE permission
    private boolean havePermission() {
        boolean read_files = checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                                 == PackageManager.PERMISSION_GRANTED;
        boolean write_files = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED;

        return (read_files && write_files);
    }

    private void getPermission() {
        //ask for permission if app does not already have permission
        if (!havePermission()) {

            //array of permissions being requested
            String[] permission_requests = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE};

            //request permission
            requestPermissions(permission_requests, MY_PERMISSIONS_REQUEST_RW_EXTERNAL_STORAGE);
        }

    }

    //sets user current image to new_image
    private void setImage(Bitmap new_image) {
        //set main imageview to opened image
        currentImageView.setImageBitmap(new_image);
        currentImageView.setVisibility(View.VISIBLE);

        //make textview teling user to open image invisible
        TextView msgTextView = (TextView) findViewById(R.id.image_msg_textview);
        msgTextView.setVisibility(View.INVISIBLE);

        //set current image to new current image
        currentImage = new_image;
    }

    //create arraylist of icons
    private void createIcons() {
        icons = new ArrayList<Icon>();
        icons.add(new Icon(IconAction.OPEN  ,R.drawable.open_icon));
        icons.add(new Icon(IconAction.SAVE  ,R.drawable.save_icon));
        icons.add(new Icon(IconAction.NEW   , R.drawable.new_icon));
        icons.add(new Icon(IconAction.SHARE ,R.drawable.share_icon));
        icons.add(new Icon(IconAction.FILTER,R.drawable.filter_icon));
        icons.add(new Icon(IconAction.CROP  ,R.drawable.crop_icon));
        icons.add(new Icon(IconAction.ROTATE,R.drawable.rotate_icon));
        icons.add(new Icon(IconAction.RESIZE,R.drawable.resize_icon));
    }
}
