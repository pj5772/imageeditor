package com.pjmoat.imageeditor.imageeditor;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class SelectSizeDialog  extends Dialog  implements
        android.view.View.OnClickListener {

    public Activity c;
    public Dialog d;
    public Button enterButton;

    String TAG = "com.pjmoat.imageeditor.imageeditor";

    //used to communicate with main activity
    private OnEnterClickListener mListener;
    public interface OnEnterClickListener {
        public void onEnterClicked(int width, int height);
    }

    public SelectSizeDialog(Activity a,OnEnterClickListener onEnterClickListener) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;

        mListener = onEnterClickListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.select_size_dialog_layout);

        //setup enter button
        enterButton = (Button) findViewById(R.id.enter_button);
        enterButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        //exit if user pressed enter
        if (v.getId() == R.id.enter_button) {

            //get reference to width and height textview
            TextView widthTextView = (TextView) findViewById(R.id.width_textview);
            TextView heightTextView = (TextView) findViewById(R.id.height_textview);

            //exit if width of height textviews are empty
            if (widthTextView.getText().length() * heightTextView.getText().length() == 0)
                return;

            //get width and height
            int width = Integer.parseInt(widthTextView.getText().toString());
            int height = Integer.parseInt(heightTextView.getText().toString());

            //both width and height should be greater than 0, otherwise exit
            if (width*height == 0)
                return;

            //call listener
            mListener.onEnterClicked(width,height);

            dismiss();
        }
    }
}