package com.pjmoat.imageeditor.imageeditor;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class CompressImageTask  extends AsyncTask<Bitmap, Void, String> {

    String TAG = "com.pjmoat.imageeditor.imageeditor";

    String path = Environment.getExternalStorageDirectory() + File.separator + "temporary_file.jpg";

    @Override
    protected String doInBackground(Bitmap... params) {
        Bitmap img = params[0];

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        img.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

        File f = new File(path);
        try {
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
        } catch (IOException e) {
            Log.e(TAG,"error while writing compressed image to memory");
        }
        return "Executed";
    }

    @Override
    protected void onPostExecute(String result) {}

    @Override
    protected void onPreExecute() {}

    @Override
    protected void onProgressUpdate(Void... values) {}
}
