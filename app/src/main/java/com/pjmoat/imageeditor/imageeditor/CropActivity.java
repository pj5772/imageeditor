package com.pjmoat.imageeditor.imageeditor;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import com.pjmoat.imageeditor.imageeditor.R;

import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

import static com.pjmoat.imageeditor.imageeditor.SeekbarType.OFFSETX;

enum SeekbarType {
    OFFSETX,OFFSETY,WIDTH,HEIGHT
};

public class CropActivity extends AppCompatActivity {

    //holds where user started dragging
    //  {-1,-1} -> user is not dragging
    float[] dragStart = {-1,-1};

    //reference to imageview containing current image
    ImageView currentImageView;

    String TAG = "com.pjmoat.imageeditor";

    //hold current image user is working  on
    Bitmap currentImage;

    //if holds image with box around selected image
    Bitmap boxedImage;

    //seekbars
    private SeekBar offsetXSeekbar,offsetYSeekbar,widthSeekbar,heightSeekbar;

    //TextViews
    private TextView offsetXTextView,offsetYTextView,widthTextView,heightTextView;

    //holds current state of each seekbar
    private int offsetX,offsetY,width,height;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crop);

        //hide actionbar
        getSupportActionBar().hide();

        //create imageview
        currentImageView = (ImageView) findViewById(R.id.mainImageView);

        //decompress sent image
        if(getIntent().hasExtra("PATH_TO_IMAGE")) {

            //get image's path
            String img_path = (String) getIntent().getExtras().get("PATH_TO_IMAGE");

            //decode bitmap
            File imgFile = new  File(img_path);
            Bitmap loadedBitmap = null;
            if(imgFile.exists()){
                loadedBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            }

            //set to current image
            if (loadedBitmap != null) {
                setCurrentImage(loadedBitmap);
            }
        }

        //setup current image
        currentImageView.setImageBitmap(currentImage);

        //setup onProgress changed for all seekbars
        offsetXSeekbar = (SeekBar) findViewById(R.id.offsetXSeekBar);
        offsetXSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                onBoxParamsChanged(SeekbarType.OFFSETX,progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        offsetYSeekbar = (SeekBar) findViewById(R.id.offsetYSeekBar);
        offsetYSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                onBoxParamsChanged(SeekbarType.OFFSETY,progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        widthSeekbar = (SeekBar) findViewById(R.id.widthSeekBar);
        widthSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                onBoxParamsChanged(SeekbarType.WIDTH,progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        heightSeekbar = (SeekBar) findViewById(R.id.heightSeekBar);
        heightSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                onBoxParamsChanged(SeekbarType.HEIGHT,progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        //setup pixel value textview
        offsetXTextView = (TextView) findViewById(R.id.offsetXText);
        offsetYTextView = (TextView) findViewById(R.id.offsetYText);
        widthTextView   = (TextView) findViewById(R.id.widthText);
        heightTextView  = (TextView) findViewById(R.id.heightText);
    }


    //called whenever any sekkbar is changed with new progress and which seekbar changed
    private void onBoxParamsChanged(SeekbarType seekbarType, int newProgress) {
        //update progress
        switch (seekbarType) {
            case OFFSETX:
                offsetX = progressToX(newProgress);
                break;
            case OFFSETY:
                offsetY = progressToY(newProgress);
                break;
            case WIDTH:
                width = progressToX(newProgress);
                break;
            case HEIGHT:
                height = progressToY(newProgress);
                break;
        }

        //update labels
        updateLabels();

        //draw rectangle arround what user has selected
        drawRectangle(offsetX,
                      offsetY,
                offsetX + width,
                offsetY + height);
    }

    //updates all seekbars with stored offsetX, offsetY, width, height
    private void updateLabels() {
        offsetXTextView.setText(offsetX + "px");
        offsetYTextView.setText(offsetY + "px");
        widthTextView.setText(width + "px");
        heightTextView.setText(height + "px");
    }

    //draw rectangle on currentImage
    private void drawRectangle(float startX, float startY, float endX, float endY) {
        boxedImage = currentImage.copy(currentImage.getConfig(), true);

        Canvas canvas = new Canvas(boxedImage);
        Paint p = new Paint();
        p.setStyle(Paint.Style.FILL_AND_STROKE);
        p.setAntiAlias(true);
        p.setFilterBitmap(true);
        p.setDither(true);
        p.setColor(Color.RED);

        canvas.drawLine(startX, startY, endX  , startY, p);//up
        canvas.drawLine(startX, startY, startX, endY  , p);//left
        canvas.drawLine(startX, endY  , endX  , endY  , p);//down
        canvas.drawLine(endX,   startY, endX  , endY  , p);

        currentImageView.setImageBitmap(boxedImage);
        //currentImageView.draw(canvas);
    }

    //called wheren user presses enter, sends image to mainactivity
    public void onEnterClicked(View v) {
        //compress image
        CompressImageTask compressImageTask = new CompressImageTask() {
            @Override
            protected void onPostExecute(String result) {
                //when compressed call onImageCompressed
                onImageCompressed(path);
            }
        };
        compressImageTask.execute(currentImage);
    }

    //called by CompressIamgeTask instance in onEnterClicked when
    //current image is compressed. This method then sends the user
    //back to the main activity with the new image.
    private void onImageCompressed(String path) {
        //send user back to mainactivity
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("PATH_TO_IMAGE",path);
        startActivity(intent);

        //end CropActivity
        finish();
    }

    //called when user presses cancel. This method exits CropActivity
    //without making any changes to image
    public void onCancelClicked(View v) {
        finish();
    }

    //called when crop is clicked, handles actually croping the photo
    public void onCropClicked(View v) {
        //area of rectangle must be greater than zero
        if (width == 0 || height == 0) {
            Toast.makeText(this,"No region of image selected.",Toast.LENGTH_SHORT).show();
            return;
        }

        //make sure rectangle is on image
        if (offsetX >= currentImage.getWidth() || offsetY >= currentImage.getHeight()) {
            Toast.makeText(this,"Rectangle is not on image.", Toast.LENGTH_SHORT).show();
            return;
        }

        //make sure rectangle is in screen
        width  = Math.min(width ,currentImage.getWidth() - offsetX);
        height = Math.min(height,currentImage.getHeight() - offsetY);

        //crop currentImage
        Bitmap croppedImage = Bitmap.createBitmap(currentImage,offsetX,offsetY,width,height);

        //make croped image new current image
        setCurrentImage(croppedImage);

        //reset for new image
        resetParams();
        resetSeekbars();
        updateLabels();
    }

    //maps [0,100] to [0,width] for converting from progress to pixels along width
    private int progressToX(int progress) {
        return (int) ((((float) progress)/100.0)*((double) currentImage.getWidth()));
    }

    //maps [0,100] to [0,height] for converting from progress to pixels along height
    private int progressToY(int progress) {
        return (int) ((((float) progress)/100.0)*((double) currentImage.getHeight()));
    }

    //sets up current image to be newImage
    private void setCurrentImage(Bitmap newImage) {
        currentImage = newImage;
        currentImageView.setImageBitmap(currentImage);
    }

    //resets box parameters
    private void resetParams() {
        //reset params
        offsetX = offsetY = width = height = 0;
    }

    ///resets all seekbars to 0
    private void resetSeekbars() {
        offsetXSeekbar.setProgress(0);
        offsetYSeekbar.setProgress(0);
        widthSeekbar.setProgress(0);
        heightSeekbar.setProgress(0);
    }
}
