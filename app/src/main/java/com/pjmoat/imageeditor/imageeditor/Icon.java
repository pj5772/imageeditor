package com.pjmoat.imageeditor.imageeditor;

import android.graphics.Bitmap;

//types of icons
enum IconAction
{
    OPEN,SAVE,NEW,SHARE,FILTER,CROP,ROTATE,RESIZE;
}

public class Icon {
    IconAction action ;
    int img_resource;

    public Icon(IconAction action, int img_resource) {
        this.action = action;
        this.img_resource = img_resource;
    }

    public IconAction getIconAction() {
        return action;
    }

    public int getImageResource() {
        return img_resource;
    }

    @Override
    public String toString() {
        return action.toString();
    }
}
